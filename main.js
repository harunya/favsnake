function setIcon(dataURL) {
    var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
    link.type = 'image/x-icon';
    link.rel = 'shortcut icon';
    link.href = dataURL;
    document.getElementsByTagName('head')[0].appendChild(link);
}

function createCanvas(width, height) {
    var canvas = document.createElement('canvas');
    canvas.width = width;
    canvas.height = height;

    let context = canvas.getContext("2d");

    return {
        context: context,
        object: canvas,
        sizeX: width,
        sizeY: height,

        clear: function(color="#FFF"){
            this.context.fillStyle = color;
            this.context.fillRect(0, 0, this.sizeX, this.sizeY);
        },
        drawRect(x, y, w, h, c="#000"){
            this.context.fillStyle = c;
            this.context.fillRect(x, y, w, h);
        }
    }
}

let MAP_SIZE = 100;
let BLOCK_SIZE = 30;


let fps = 60;
let canvas = createCanvas(MAP_SIZE, MAP_SIZE);

let dTime = 0;

let map = [new Tile(30, 30, "#101", 30)];

let speed = 100;
var direction = 1; // 0 - left, 1 - right, 2 - up, 3 - down
var snakeParts = [];

snakeParts.push(new Tile(0, 0, "#a0c", BLOCK_SIZE));


function is_collision(data1, data2) {
    if (data1 == null || data2 == null)
        return true;

    return data1.material && data1.x < data2.x + data2.size &&
        data1.x + data1.size > data2.x &&
        data1.y < data2.y + data2.size &&
        data1.y + data1.size > data2.y;
}

let curFps = 0;
let dFps = 0;
let fpsTime = 0;

setInterval(()=>{document.getElementById("fps").innerHTML = dFps;}, 100);

function draw(){
    let time = Date.now()/1000;

    if(fpsTime+1 < time){
        fpsTime = time;
        dFps = curFps;
        curFps = 0;
    }else
        curFps++;


    canvas.clear();
    
    for(let partTile of snakeParts){
        //Draw snake part
        partTile.draw(canvas);

        //Move snake part
        partTile.x += [-1, 1, 0, 0][direction]*dTime*speed;
        partTile.y += [0, 0, -1, 1][direction]*dTime*speed;


        for(let tile of map){

            if (is_collision(partTile, tile)) {

                let hXW = tile.x + tile.size / 2;
                let hYH = tile.y + tile.size / 2;

                dx = ((partTile.x + partTile.size / 2) - hXW) / tile.size;
                dy = ((partTile.y + partTile.size / 2) - hYH) / tile.size;
                
                partTile.x += dx;
                partTile.y += dy;
                break;
            }
        }

    }

    for(let tile of map){
        tile.draw(canvas);
    }

    // Apply framebuffer
    setIcon(canvas.object.toDataURL());

    dTime = Date.now()/1000 - time;

    setTimeout(draw, 1);
}

setTimeout(draw, 1);


// Keyboard Listener
window.onkeydown = function(e){
    let key = e.key;

    if(key == "a")
        direction = 0;
    if(key == "d")
        direction = 1;
    if(key == "w")
        direction = 2;
    if(key == "s")
        direction = 3;
}