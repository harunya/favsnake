class Tile {
    material = true;

    constructor(x, y, color, size) {
        this.x = x;
        this.y = y;
        this.color = color;
        this.size = size;
    }

    draw(ctx){
        canvas.drawRect(this.x, this.y, this.size, this.size, this.color);
    }
}